"use strict";
/* eslint-disable no-unused-vars */
class Sprite {
	constructor(img, x, y, width, height) {
		this.img = img;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	draw (ctx, x, y) {
		ctx.drawImage(this.img, this.x, this.y, this.width, this.height,
			x, y, this.width, this.height);
	}
}

function createSprites(img) {
	return {
		spriteBird: [
			new Sprite(img, 312, 230, 34, 24),
			new Sprite(img, 312, 256, 34, 24),
			new Sprite(img, 312, 282, 34, 24)
		],
		spriteBg: 		new Sprite(img,   0,   0, 276, 228),
		spriteFg: 		new Sprite(img, 276,   0, 224, 112),
		spriteSplash: 	new Sprite(img,   0, 228, 118,  98),
		spriteText: {
			flappyBird: new Sprite(img, 118, 228, 192, 44),
			gameOver:   new Sprite(img, 118, 272, 188, 38),
			getReady:   new Sprite(img, 118, 310, 174, 44)
		},
		spritePipe: {
		pipeN:	new Sprite(img, 554,   0,  52, 400),
		pipeS:	new Sprite(img, 502,   0,  52, 400)
		},
		spriteEnd: new Sprite(img, 276,  112, 226, 116),
		spriteButtons: {
			rate:  new Sprite(img, 158, 354, 80, 28),
			menu:  new Sprite(img, 238, 354, 80, 28),
			share: new Sprite(img, 318, 354, 80, 28),
			score: new Sprite(img, 158, 382, 80, 28),
			ok:    new Sprite(img, 238, 382, 80, 28),
			start: new Sprite(img, 318, 382, 80, 28)
		}
	}
}