"use strict";
/* eslint-disable no-unused-vars */

//Game states enum
var States = Object.freeze({
	Splash: 0, Playing: 1, End: 2
});

class GameArea {
	constructor() {
		this._canvas = document.getElementById("game_canvas");
		this.ctx = this._canvas.getContext("2d");
		this.ctx.fillStyle = "#70c5ce";
		this.width = this._canvas.width;
		this.height = this._canvas.height;
	}
}

class Game {
	constructor(spr) {
		this.currState = States.Splash;
		this._area = new GameArea();
		this.ctx = this._area.ctx;
		this.sprites = spr;
		this.frames = 0;
		this.score = 0;
		this.bestScore = 0;
		this._fgPos = 0;	//foreground pos, to simulate move of foreground
		this.okBtnX = 0;	//ok button coords
		this.okBtnY = 0;
		this.gameSpeed = 11;
		this.frameTime = 0;
		this.deltaT = 0;
		this.birdTimeAcc = 0;
		this.birdAnimFreq = 80;
		this.bird = new Bird(this.sprites.spriteBird, 
							 this._area.width, 
							 this._area.height);
		this.pipes = [];
		this.pipeSpacing = 150;
		this.pipeStartPos = 500;
		this.initPipes();
		/*			EVENT LISTENERS			*/
		this._area._canvas.addEventListener("mousedown", this.mouseClick.bind(this), false);
		this._area._canvas.addEventListener("touchstart", this.mouseClick.bind(this), false); //for mobile
		window.addEventListener("keydown", this.keyDown.bind(this), false); //for space
	}

	initPipes() {
		for (let i = 0; i < 3; i++) {
			this.pipes.push(new Pipe(
				this.sprites.spritePipe, 
				this._area.height, 
				this.sprites.spriteFg.height,
				this.pipeStartPos + (i*this.pipeSpacing)))
		}
	}

	resetPipes() {
		for (let i = 0; i < this.pipes.length; i++) {
			this.pipes[i].reset(this.pipeStartPos + (i*this.pipeSpacing));
			
		}
	}

	updatePipes() {
		var prev = this.pipes[this.pipes.length-1];
		this.pipes.forEach(element => {
			if (element.update(this.gameSpeed * this.deltaT) === true){
				element.reset(prev.x + this.pipeSpacing);
			}
			prev = element;
		});
	}

	collisionPipes() {
		var collided = false;

		this.pipes.forEach(el => {
			//Check scoring
			if (this.bird.x >= el.x && el.isScored === false) {
				el.isScored = true;
				this.score++;
			}
			//Check circle to box collision, math stuff
			var dX = this.bird.x - Math.min(el.x + el.width, Math.max(this.bird.x, el.x));
			var dY = this.bird.y - Math.min(el.y + el.height, Math.max(this.bird.y, el.y));
			var dY2 = this.bird.y - Math.min(el.y + 2 * el.height + el.hole, Math.max(this.bird.y, el.y + el.height + el.hole));
			var rPow = this.bird.radius*this.bird.radius;

			if (dX*dX + dY*dY < rPow || dX*dX + dY2*dY2 < rPow) {
				collided = true;
			}
		});
		return collided;
	}

	collisionSolver() {
		var collided = this.collisionPipes();
		if (collided === true) {
			this.currState = States.End;
		}
	}

	drawPipes() {
		this.pipes.forEach(element => {
			element.draw(this.ctx);
		});
	}

	moveFg() {	
		if (this.currState !== States.End) {
			this._fgPos = Math.floor(( this._fgPos - (this.gameSpeed * this.deltaT) )) % 14;
		}
	}

	collisionFg() {
		var height = this._area.height - this.sprites.spriteFg.height - 10;
		if (this.bird.y >= height ) {
			this.bird.y = height;
			this.bird.speed = this.bird.jump;
			this.currState = States.End;
		}
	}

	resetGame() {
		this.resetPipes();
		this.bird.reset();
		this.currState = States.Splash;
		this.score = 0;
	}

	update() {
		this.frames++;
		this.moveFg();
		if (this.currState === States.Playing) {
			this.updatePipes();
		}
		if (this.birdTimeAcc >= this.birdAnimFreq) {
			this.bird.updateAnim();
			this.birdTimeAcc -= this.birdAnimFreq;
		}
		this.bird.update(this.frames, this.currState, this.deltaT);
		this.collisionFg();
		this.collisionSolver();
	}

	getCenteredX(length) {
		return Math.floor( this._area.width / 2 ) - Math.floor(length / 2);
	}

	drawBg() {
		var w = this.sprites.spriteBg.width;
		var h = this.sprites.spriteBg.height;
		var aH = this._area.height;

		this.ctx.fillRect(0, 0, this._area.width, aH);

		this.sprites.spriteBg.draw(this.ctx, 0, aH - h);
		this.sprites.spriteBg.draw(this.ctx, w, aH - h);
		
	}

	drawFg() {
		var w = this.sprites.spriteFg.width;
		var h =  this.sprites.spriteFg.height;
		var aH = this._area.height;
		this.sprites.spriteFg.draw(this.ctx, this._fgPos, aH - h);
		this.sprites.spriteFg.draw(this.ctx, this._fgPos+w, aH - h);
	}

	drawSplash() {
		var x = this.getCenteredX(this.sprites.spriteSplash.width);
		var y = this._area.height - 350;
		this.sprites.spriteSplash.draw(this.ctx, x, y);
		x = this.getCenteredX(this.sprites.spriteText.getReady.width);
		y -= 100;
		this.sprites.spriteText.getReady.draw(this.ctx, x, y);
	}

	drawScore() {
		this.ctx.save();
		this.ctx.fillStyle = "white";
		this.ctx.font = "45px arial";
		this.ctx.textAlign = "center";
		this.ctx.fillText(this.score, this._area.width/2, this._area.height/8);
		this.ctx.restore();
	}

	drawEnd() {
		//ScoreScreen
		var x = this.getCenteredX(this.sprites.spriteEnd.width);
		var y = this._area.height - 350;
		this.sprites.spriteEnd.draw(this.ctx, x, y);
		//Button
		this.okBtnY = y + this.sprites.spriteEnd.height + 10;
		this.okBtnX = this.getCenteredX(this.sprites.spriteButtons.ok.width);
		this.sprites.spriteButtons.ok.draw(this.ctx, this.okBtnX, this.okBtnY);
		//Text
		x = this.getCenteredX(this.sprites.spriteText.gameOver.width);
		y = this._area.height - 400;
		this.sprites.spriteText.gameOver.draw(this.ctx, x, y);
	}

	render() {
		this.drawBg();
		this.drawPipes();
		this.bird.draw(this.ctx);
		this.drawFg();
		if (this.currState === States.Splash) {
			this.drawSplash();
		}
		else if (this.currState === States.End) {
			this.drawScore();
			this.drawEnd();
		}
		else {
			this.drawScore();
		}
	}

	checkOkBtn(evt) {
		var 
		//Coords of mouse ptr on site
		mX = evt.clientX - this._area._canvas.offsetLeft, 
		mY = evt.clientY - this._area._canvas.offsetTop,
		//Scaling factor of canvas
		sX = this._area.width / this._area._canvas.clientWidth,
		sY = this._area.height / this._area._canvas.clientHeight,
		//mouse coords to canvas coords
		mcX = mX * sX,
		mcY = mY * sY,

		width = this.sprites.spriteButtons.ok.width,
		height = this.sprites.spriteButtons.ok.height;

		// check if inside the button
		if (this.okBtnX < mcX && mcX < this.okBtnX + width &&
			this.okBtnY < mcY && mcY < this.okBtnY + height
		) {
			this.resetGame();
		}
	}

	handleInput(evt) {
		switch (this.currState) {
			case States.Splash:
				this.currState = States.Playing;
				this.bird.jumping(this.deltaT);
				break;
			case States.Playing:
				this.bird.jumping(this.deltaT);
				break;
			case States.End:
				this.checkOkBtn(evt);
				break;
			default:
				break;
		}
	}

	keyDown(event) {
		if (event.code == "Space") {
			event.preventDefault();
			this.handleInput(event);
		}
	}

	mouseClick(event) {
		this.handleInput(event);
	}

	//main loop
	run(timeStamp) {
		if (timeStamp) {
		var passed = timeStamp - this.frameTime; //time passsed between frames
		this.deltaT = passed / 100;
		this.birdTimeAcc += passed;
		this.frameTime = timeStamp;
		this.update();
		this.render();
		}
		requestAnimationFrame(this.run.bind(this));
	}
}

class Bird {
	constructor(spr, w, h) {
		this._sprite = spr;
		this._areaWidth = w;
		this._areaHeight = h;
		this.x = Math.floor(w/4);		//divided by nth of the screen space
		this.y = Math.floor(h/3);
		this.radius = 12;
		this.animTick = 0;				//to specify which sprite in animation to draw
		this.speed = 0;					
		this.gravity = 1.98;			//prev 0.07
		this.jump = 4.7;				//prev 2.4
		this.rotation = 0;
		this.animSeq = [0, 1, 2, 1];	//sprite draw order
		this._animAcc = 0;				//animation counter accumulator
	}

	updateAnim() {
		this.animTick = this.animSeq[ (this._animAcc % 4 )];
		this._animAcc++;
	}

	reset() {
		this.x = Math.floor(this._areaWidth/4);		
		this.y = Math.floor(this._areaHeight/3);
	}

	update(frames, state, deltaT) {
		if (state === States.Splash) {
			this.hovering(frames);
		}
		else { //TODO: Maybe  put 3.0 and 0.3 to vars?
			this.speed = this.speed <= 6.0  ? (this.speed + this.gravity * deltaT)  : this.speed;
			this.y += this.speed * deltaT * 6;
			if (this.speed  >= this.jump) {
				this.animTick = 1;
				this.rotation = Math.min(Math.PI/2, this.rotation + 0.3);
			}
			else {
				this.rotation = -0.3;
			}
		}
	}

	hovering(frames) {
		this.y = this.y + 0.25*Math.cos(frames/20);	//math magic
		this.rotation = 0;
	}

	draw(ctx) {
		var i = this.animTick;
		ctx.save();
		ctx.translate(this.x, this.y);
		ctx.rotate(this.rotation);
		this._sprite[i].draw(ctx, -this._sprite[i].width/2, -this._sprite[i].height/2);

		//Collision cricle draw - for presenting
		// ctx.fillStyle = "red";
		// ctx.beginPath();
		// ctx.arc(0, 0, this.radius, 0, 2*Math.PI);
		// ctx.fill();

		ctx.restore();
	}

	jumping(deltaT) {
		this.speed = -this.jump;
	}
}

class Pipe {
	constructor(spr, h, fgH, x) {
		this._sprite = spr;
		this._areaHeight = h;
		this.fgH = fgH;
		this.isScored = false;
		this.height = this._sprite.pipeS.height;
		this.width = this._sprite.pipeS.width;
		this.x = x;
		this.y = 0;
		this.rollHeight();
		this.hole = 85;
	}

	reset(newX) {
		this.isScored = false;
		this.rollHeight();
		this.x = newX;
	}

	update(speed) {
		this.x = Math.floor (this.x - speed);
		//Check if out of canvas
		if (this.x < -this.width ) {
			return true;
		}
		return false;
	}

	draw(ctx) {
		var ySprN = this.y+this.hole+this.height;
		this._sprite.pipeN.draw(ctx, this.x, this.y);
		this._sprite.pipeS.draw(ctx, this.x, ySprN);
	}

	rollHeight() {
		this.y = this._areaHeight - (this.height+this.fgH+120+200*Math.random());
	}
}

function main() {
	var img = new Image();
	var game;

	img.onload = function() {
		game = new Game(createSprites(this));
		game.run();
	};

	img.src = "./textures/atlas.png";
}

main();