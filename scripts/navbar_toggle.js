"use strict";
// eslint-disable-next-line no-unused-vars
function toggleNav() {
	var links = Array.from(document.querySelectorAll(".main-navbar > a"));
	if (links[0].style.display == "none") {
		links.forEach(element => {
			element.style.display = "block";
		});
	}
	else {
		links.forEach(element => {
			element.style.display = "none";
		});
	}
}
